# ------------------ PART 2 Stocks ---------------

import pandas as pd
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os

# We read the CSV files provided of the seven stocks
StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockIBM, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])


# We only take into account business days by excluding week-ends andUS holidays
# We still have an offset of 7 days, going up to 08/01/2021 instead of 01/01/2021
# We have put an  # for every print(.) to ease the running of the code
us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())

GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq= us_bd)
# print(GOOGL)

FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq= us_bd)
# print(FDX)

IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq= us_bd)
# print(IBM)

KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq= us_bd)
# print(KO)

MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq= us_bd)
# print(MS)

NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq= us_bd)
# print(NOK)

XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq= us_bd)
# print(XOM)


# We plot the evolution of the stock prices of each company on the same graph
plt.plot(GOOGL['date'], GOOGL['high'], label="Google")
plt.plot(FDX['date'], FDX['high'], label="FDX")
plt.plot(IBM['date'], IBM['high'], label="IBM")
plt.plot(KO['date'], KO['high'], label="KO")
plt.plot(MS['date'], MS['high'], label="MS")
plt.plot(NOK['date'], NOK['high'], label="NOK")
plt.plot(XOM['date'], XOM['high'], label="XOM")
plt.xlabel('Dates')
plt.ylabel('Stock price')
plt.legend()
plt.title('Stock prices evolution')
plt.show()


# We will define the class Stocks
class Stocks:
    def __init__(self, term, ticker, amount, date):
        self.term = term
        self.ticker = ticker
        self.amount = amount
        self.date = datetime.strptime(date, "%Y-%m-%d")
        self.StartDate = self.date
        self.EndDate = self.date - timedelta(days=(-self.term))
        self.Number = round((self.amount / (self.ticker.loc[XOM["date"] == self.date, "high"])), 4)

# We define the function that provides the date
    def dateinvest(self):
        while self.date not in set(self.ticker['date']):
            self.date = self.date - timedelta(days=1)
        else:
            self.date = self.date
            return self.date

# We explicit the price to be used
    def price(self):
        return self.ticker.loc[XOM["date"] == self.date, "high"]

# We compute the return (in volume) on an investment
    def returninvol(self):
        pricebuy = float(self.ticker.loc[XOM["date"] == self.StartDate, "high"]) * self.Number
        pricesell = float(self.ticker.loc[XOM["date"] == self.EndDate, "high"]) * self.Number
        return round((pricesell - pricebuy), 4)

# We compute the return (in percentage) on an investment
    def returninpercent(self):
        pricebuy = float(self.ticker.loc[XOM["date"] == self.StartDate, "high"]) * self.Number
        pricesell = float(self.ticker.loc[XOM["date"] == self.EndDate, "high"]) * self.Number
        return round((pricesell/pricebuy - 1) * 100)


# Test n°1 : We print the return on investment for a company, in this case NOK
InvestmentNOK = Stocks(91, NOK, 1000, "2016-09-01")

print("The date of the investment is the " + str(InvestmentNOK.dateinvest()))
print("The return of the investment in volume is " + str(InvestmentNOK.returninvol()))
print("The return of the investment in percentage is " + str(InvestmentNOK.returninpercent()))


# Test n°2 : We print he return on investment for a company, in this case FDX
InvestmentFDX = Stocks(91, FDX, 1000, "2017-12-04")

print("The date of the investment is the " + str(InvestmentFDX.dateinvest()))
print("The return of the investment in volume is " + str(InvestmentFDX.returninvol()))
print("The return of the investment in percentage is " + str(InvestmentFDX.returninpercent()))