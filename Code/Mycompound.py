def maths(initial, interestrate, years, compoundsperyear):
    result = initial * ((1 + ((interestrate / 100) / compoundsperyear)) ** (years * compoundsperyear))
    return result


Allresults = [s for s in range(0, 50)]

ShortTerm = [maths(initial=250, interestrate=1.5, years=s, compoundsperyear=1) for s in range(0, 50)]
LongTerm = [maths(initial=1000, interestrate=3, years=s, compoundsperyear=1) for s in range(0, 50)]

print(ShortTerm)
print(LongTerm)

st, = plt.plot(Allresults, ShortTerm)
lt, = plt.plot(Allresults, LongTerm)
plt.title("Short term and Long term Bond")
plt.xlabel("Number of Years")
plt.ylabel("Balance in €")
plt.xlim(0, 50)
plt.ylim(200, 4500)
plt.legend([st, lt], ["Short term", "Long Term"])
plt.show()

