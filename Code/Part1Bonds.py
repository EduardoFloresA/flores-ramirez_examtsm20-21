# ------------------ PART 1 Short and Long Term Bond ---------------

# We will import matplotlib in order to make a graph
import matplotlib.pyplot as plt


# We create our Class called "Bond"
class Bond(object):
    def __init__(self, initial, years, compoundsperyear):
        self.initial = initial
        self.years = years
        self.compoundsperyear = compoundsperyear

    def bondrate(self):
        return False

    def maths(self):
        returnbond = self.initial * ((1 + ((self.bondrate() / 100) / self.compoundsperyear))
                                     ** (self.years * self.compoundsperyear))
        return returnbond


# We create our Class "Short Bond"
class Shortbond(Bond):
    def bondrate(self):
        r = 1.5
        return r


# We create our Class "Long Bond"
class Longbond(Bond):
    def bondrate(self):
        r = 3
        return r


# We enter our values in order to create the Plot
st = Shortbond(250, 50, 1)
lt = Longbond(1000, 50, 1)


# We create for loops in order to plot the evolution of the bonds in a 50 year period
pricest = [st.initial * ((1 + ((st.bondrate() / 100) / st.compoundsperyear)) ** (i * st.compoundsperyear))
           for i in range(1, 51)]
pricelt = [lt.initial * ((1 + ((lt.bondrate() / 100) / lt.compoundsperyear)) ** (i * lt.compoundsperyear))
           for i in range(1, 51)]


# We plot both the short term and long term bonds
shortt, = plt.plot(range(1, 51), pricest)
longt, = plt.plot(range(1, 51), pricelt)
plt.title("Short term and Long term Bond")
plt.xlabel("Number of Years")
plt.ylabel("Balance in €")
plt.xlim(0, 52)
plt.ylim(200, 4500)
plt.legend([shortt, longt], ["Short term", "Long Term"])
plt.show()
