# ------------------ PART 3 & 4 Investors & Simulations ---------------

import os
import pandas as pd
import numpy as np
from random import choice, uniform, randint, random
from Part2Stocks import Stocks
from Part1Bonds import Bond, Longbond, Shortbond
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay


StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockIBM, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])

# We only take into account business days by excluding week-ends andUS holidays
# We still have an offset of 7 days, going up to 08/01/2021 instead of 01/01/2021
us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())
GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq=us_bd)
FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq=us_bd)
IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq=us_bd)
KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq=us_bd)
MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq=us_bd)
NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq=us_bd)
XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq=us_bd)

# First of all we will define a vector of each stocks:
stocks = {"FDX": FDX, "GOOGL": GOOGL, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}


# We will define a class investor and 3 subclass for each type of investors:
class Investors(object):
    def __init__(self, date, term, budget):
        self.date = date
        self.term = term/365
        self.budget = budget


# We define variables to be used
ShortBond_MinInvest = 250
ShortBond_MinTerm = 2
LongBond_MinInvest = 1000
LongBond_MinTerm = 5


# We create a sub-class of Investors : Defensive investors.
# We assume that a defensive investor has 50% probability to invest in Short Term Bonds or 50% in Long term Bonds.
class Defensive(Investors):
    def investdefensive(self):
        n = random()
        print("The value of the random variable is " + str(n))

        while self.budget >= ShortBond_MinInvest:
            if n > 0.5 and self.budget >= 1000:
                simul = Longbond(self.budget, self.term, 1)
                returndef = simul.maths()
            else:
                simul = Shortbond(self.budget, self.term, 1)
                returndef = simul.maths()
            return returndef


# We test the Defensive investment
testDefensive = Defensive("2016-09-01", 365, 5000)
print("The return of the defensive investor is " + str(testDefensive.investdefensive()) + " dollars when investing "
      + str(testDefensive.budget) + " dollars, for " + str(testDefensive.term) + " year(s) maturity.")


# We now create the sub-class Aggressive
class Aggressive(Investors):
    def investaggressive(self):
        randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}
        returnoninvestment = 0
        while self.budget >= 100:
            stocks_rand = choice(list(randomstock))
            budget_rand = uniform(0, self.budget)
            shares = Stocks(self.term, budget_rand, randomstock[stocks_rand], self.date)
            self.budget = self.budget - budget_rand
            returnoninvestment = returnoninvestment + round(shares.returninvol(), 2)
            print(shares)
        return round(returnoninvestment * float(self.budget), 2)


# We test the Aggressive investment
testAggressive = Aggressive("2016-09-01", 365, 5000)
print(testAggressive.investaggressive())


# We now create the sub-class Mixed investor
class Mixed(Investors):
    def investmixed(self):
        n = random()
        print("The value of the random variable is " + str(n))
        m = random()
        print("The value of the random variable is " + str(m))
        while self.budget >= ShortBond_MinInvest:
            if n > 0.25:
                randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}
                returnoninvestment = 0
                while self.budget >= 100:
                    stocks_rand = choice(list(randomstock))
                    budget_rand = uniform(0, self.budget)
                    shares = Stocks(self.term, budget_rand, randomstock[stocks_rand], self.date)
                    self.budget = self.budget - budget_rand
                    returnoninvestment = returnoninvestment + round(shares.returninvol(), 2)
                    print(shares)
                return round(returnoninvestment * float(self.budget), 2)
            else:
                if m > 0.5 and self.budget >= 1000:
                    simul = Longbond(self.budget, self.term, 1)
                    returnmix = simul.maths()
                else:
                    simul = Shortbond(self.budget, self.term, 1)
                    returnmix = simul.maths()
            return returnmix


# We test the Defensive investment
testMix = Mixed("2016-09-01", 365, 5000)
# print("The return of the mixed investor is " + str(testMix.investmixed()) + " dollars when investing "
#      + str(testMix.budget) + " dollars, for " + str(testMix.term) + " year(s) maturity.")


# We model 500 individuals for each type of investors with a starting budget of 5000$
nb_simulation = 500
budget = 5000

defensive_invest = np.zeros(nb_simulation)
aggressive_invest = np.zeros(nb_simulation)
mixed_invest = np.zeros(nb_simulation)

# For loop in order to simulate the different investors
for i in range(nb_simulation):
    Defensive = Investors(budget, "defensive")
    Aggressive = Investors(budget, "aggressive")
    Mixed = Investors(budget, "mixed")
    defensive_invest[i] = Defensive.investdefensive()
    aggressive_invest[i] = Aggressive.investaggressive()
    mixed_invest[i] = Mixed.investmixed()

avg_defensive_capital = np.mean(defensive_invest)
avg_aggressive_capital = np.mean(aggressive_invest)
avg_mixed_capital = np.mean(mixed_invest)

avg_defensive_return = avg_defensive_capital/budget - 1
avg_aggressive_return = avg_aggressive_capital/budget - 1
avg_mixed_return = avg_mixed_capital/budget - 1


# Simulation test with the defensive investor
print("the average capital of 500 defensive investors at the end of the investment period is" +
      round(avg_defensive_capital) + "i.e" + (round(avg_defensive_return, 4)*100) + "%")



