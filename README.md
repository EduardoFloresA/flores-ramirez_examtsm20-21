# Project Title

Investors Simulation

## Getting Started

Our Python project consisted on an investor simulation. First of all, we had to model the financial instruments in which the investor will invest. We started with the Bonds and stating all their characteristics as their minimum holding term, a minimum amount to enter into the transaction and their yearly interest rate. We had two types of bonds, short term and long term that correspond to a minimum holding term of 2 years and 5 years respectively. 
The second financial instrument that we had to model were Stocks. We had 7 different United States stocks, from 7 different industries. For this financial asset we needed to consider different attributes as the holding period, the number of shares bought given a certain invested amount and the date on which they we’re bought. We needed to consider that stocks can only be bought in business days. On the assumptions we have to take into account that we will take the highest price as our quoted price. This can be misleading for our returns as a stock can reach very high prices but close at a negative return. We also need to contemplate that we will assume no transaction a cost on each trade.
Once the instruments completed, we had to define 3 different types of investors. Aggressive, holding only high-risk assets as stocks. Defensive, holding “risk free” assets as Bonds and finally mixed, holding both stocks and bonds. Each one of them will detain a certain proportion of assets with an initial budget of 5000$. Finally, with the use of object-oriented programming we will simulate the different types of investors and making different scenarios changing the proportions.
Moreover, in Appendix 1, you will find the timestamp of our Bitbucket submission. The link to the repository is the following: https://bitbucket.org/EduardoFloresA/flores-ramirez_examtsm20-21/

### Prerequisites

You need to install different packages as:
import pandas as pd
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os

### Part 1

To begin the project, we wanted to set up properly our repository. The first misunderstanding that we encounter was between to create a whole new repository to work on or to fork the repository given by the teacher and start working on it. We decided to take the repository given, adapt it and put in the structure viewed in class: Code, Data, Documentation and Results that we can see on Appendix 2.
We wanted to make sure that we gave the right access to our peer, that’s why we tested the methodology Commit, Push and Pull a few times to make sure that everything was settled rightly. When we started working on it, we had a few problems with the commit and push as we didn’t know that we were working on it at the same time. That’s why we had merging errors appearing when the other person tried to pull the work. We were working on different versions, not the updated ones. For the following weeks we tried to work at different moments and each time that someone made a major change we notify the other to pull the work. For those reasons we also create a draft repository where we tested our codes avoiding damaging the project repository.
We begin implementing the computations to have the return of the short term and long-term bond for 1 year in the file Mycompound.


We created a function maths to express the characteristics of the bonds. In order to make the plot over a period of 50 years we use list comprehension and search in matplotlib on the internet to find a good way to visualize our plot. 
For the next step we wanted to have a good structure to be able to adjust it more easily for the next steps, we create the file Part1Bonds.Before starting we draw the structure of the code containing the different object and class hierarchy see in Appendix 3. We struggle at the beginning to find a good way to implement object-oriented programming to our code. We knew that the Short-term bond and Long-term Bond had almost the same characteristics as initial amount investment, minimum price of the bond, the term and the yearly interest rate. Therefore, we decided to create the Short-term bond and Long-term bond class with a fix attribute being the interest rate r 1.5% and 3% respectively.
Once we found the good way to implement OOP to our code, we combine the plot made before to represent the different bonds investments.
On the plot in Appendix 4, we can see that there is a significant difference between both bonds. At the beginning we see that there is a difference of 750$ of the initial amount invested. However, 50 years after the difference between investments is 3857$ (4383$- 526$). This is due to the effect of the compounded interest rate.


## Part 2

For part 2 we tried to follow the same approach. We first tried to make the code and having the results and then combine those results in an OOP way. After doing the project we might think that this wasn’t the best approach. We maybe would have same more time if we started thinking right away in an OOP structure (see Appendix 5). We needed first to read the data provided in CSV file by the teacher with the package pandas.
We started making the plot (see Appendix 6) for the different stocks using “High” as column for the stock prices. We found out that the x axes were represented with numbers and not with actual dates. So, it took us a long time in order to figure out how to manage the dates all along the code. The first issue we had was converting numbers into dates and the second one was to take the closest possible business day before the given date. We began with if statements. For example, if day-1 is a Saturday then take day-2. Or if day -1 was a Friday then take Friday. It was taking too much time with trials and errors in order to get a proper code respecting the OOP. That’s why we decided to take a calendar that will only contain business days. Called CustomBusinessDay where we will have all the US Federal Holiday Calendar.  We imported pandas.tseries.holiday for USFederalHolidayCalendar and pandas.tseries.offsets for CustomBusinessDay. However, after trying this method we notice that we had an offset of one week. For example, our last week instead of being to the 01/01/2021 is until the 08/01/2021. We thought about correcting this error by downloading directly the data from Yahoo Finance including the dates and not a number. Nevertheless, we decided to kept the approach before as we were employing the data given by the teacher.  
After arranging our data with the corresponding dates, we were unable to display the plot in the correct way.
The next challenge was to create the Class stocks with his respective property in order to get the price of the stock at a given date. To create a return on investment we created 2 dates the price of the stock when is bought and when is sold. Then we compute the difference between both with the following formula.
 
Again, this was difficult to handle because of the dates. We had to create a function date with something that we called “time delta” in order to compute the period of investment.
At the end of the code, we print the price, the date and the return in percentage given the input period.


### Part 3

In order to create part 3, we needed to start combining our bonds and stock’s structure. First of all, we imported our “Part1Bonds” and our “Part2Stocks” to be able to used them in the new file Part3. 
This refers to the Libraries and Modules course part, where we can split the code in multiple files and classes to keep a good structured and easy to handle. First of all, we started creating a class Investors, this will be our (Object) in the code. It will be contained by the constituents Date, Term and Budget. Before starting with the different types of investors we define the minimum investment for Short term bonds and Long-Term Bonds being 250 and 1000 respectively. As well as their minimum holding term.
We will represent the class hierarchy in Appendix 7 for the Investors with Aggressive, Defensive and Mixed. We knew that we needed to create a list including all the possible stocks where the investor could invest in. We built this list linked to a function called “choice”.
Therefore, we also imported a random package in Python that contains this function “choice”. It will enable us to retrieve a random element from a non-empty sequence, in our case the option to choose from our 7 different stocks. We also used the function uniform in random. This function will produce a random number between 0 and our budget 5000. The procedure will be as follows. First, the code will select a random stock from the list. Second, it will set up a certain budget for this specific stock and it will buy a given number of shares. Third, this process will be repeated while the budget of the investor is bigger than 100.
For the defensive investor, we decided to create a random number that will give us a probability “n” between 0 and 1. We assumed that the investor has 50% probability to invest in short term bonds or in long term bonds. In fact, we didn’t manage to create a loop where the procedure will be executed multiple times until the investor had not enough remaining budget. That’s why we decided to put in place a simpler assumption as we explained before. So, for example, if n is higher than 0.5 then the investor will invest all his budget in long term bonds. The procedure is the same for short term bond in case for n lower than 0.5.
For the mixed investor we followed the same approach and the same assumptions as we explicit before. We made the hypothesis that he will have 25% chance investing in bonds and 75% in stocks. Then, we created 2 random variables n and m. The random variable m, at a second stage, will tell us if the investor will invest in short term bonds or long-term bonds. We acknowledge that the code for the mix investor could be more optimal in OOP. Like calling for the classes that we created before for the aggressive and the defensive but we encounter certain problems to link our code to be more optimal and to run it efficiently. We know that our problem might be connected to our Part2 where we created the class “Stocks” and the linkage within our stock properties. 
We encounter real difficulties to be able to finish this part of the code. When printing our results, we found multiple errors like float object has no attribute loc. In order to resolve our problem, we tried to look out for information in the internet and apply it to us. Unfortunately, we didn’t manage to fix the error and it affect it our last part of the code where we expected to have our 500 simulations and the part 4. We put it in place the structure that this kind of simulation should have. Nevertheless, we realize that we won’t have the correct answer as we explained before. 


### Conclusion

We were pleased to work on this Python project. It allowed us to push ourselves to get deeply involved on the drawing, making and testing of a financial code. It also helped us to understand better the errors and mistakes that we can encounter on our future jobs respecting to coding. Having said that, we also felt that in some cases we would have liked to have more guidance in order to finish, or maybe structure, some parts of the code. As we explained before on the report, we encounter some difficulties to solve certain problems. If I had to do a similar project in the future, I would try to look the whole goal of the code at the beginning before starting writing.

## Authors

Daniel Eduardo Ramirez and Eduardo Flores

